﻿using System;
using System.Text.RegularExpressions;

namespace Password_Validation
{
    internal class PasswordValidation
    {
        //pattern for Password Validation
        public const string pattern = @"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{5,}$";

        //Create a Function to Validate Password
        public static bool ValidatePass(string pass)
        {
            if(pass!=null) 
                return Regex.IsMatch(pass, pattern);
            else 
                return false;
        }
    }
}
