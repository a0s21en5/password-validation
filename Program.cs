﻿//Only alphanumeric inputs are accepted in the password field.
//It should start with the uppercase alphabet.
//At Least one uppercase alphabet password.

using Password_Validation;

Console.WriteLine("Enter Your Password: ");
string Store_Pass = Console.ReadLine();
bool ValidationStatus = PasswordValidation.ValidatePass(Store_Pass);
if (ValidationStatus)
{
    Console.WriteLine("Your Password is valid");
}
else
{
    Console.WriteLine("Re-try with a different Password");
}